# piano-toolbar-js

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/piano-toolbar-js.svg)](https://www.npmjs.com/package/piano-toolbar-js) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install -D git+https://bitbucket.org/evidentli/piano-toolbar-js.git
```

## Usage

For use with the default set of Piano analytic tools (will automatically apply the icon/names):

```jsx
import React, { Component } from 'react'

import {Toolbar} from 'piano-toolbar-js'

class Example extends Component {
  render() {
    return <Toolbar toolType={'banjo'}/>
  }
}
```

See the [config](./src/config.js) file to configure the tool names/icons etc.

For general use:
```jsx 
<Toolbar toolType={'piano'} toolName={'Piano'} nodeName={'piano'}/>
```

See [examples](./example) for more information

## License

MIT © [](https://github.com/)
