This example was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It is linked to the piano-toolbar-js package in the parent directory for development purposes.

You can run `npm install` and then `npm start` to test your package.
