import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';

import { Toolbar } from 'piano-toolbar-js';
import 'piano-toolbar-js/dist/index.css';

const sleep = ms => new Promise(r => setTimeout(r, ms));

const App = () => {
    const [version, setVersion] = React.useState(1);

    return <div style={{backgroundColor: "#f4f4f5"}}>
        <select onChange={e => setVersion(parseInt(e.target.value))}>
            <option value={1}>Version 1</option>
            <option value={2}>Version 2</option>
        </select>
        <Toolbar toolType={"piano"} nodeName={"piano"} version={version}/>
        <br/>
        <Toolbar toolType={"piano"} toolName={"Piano"} nodeName={"piano"} version={version}/>
        <br/>
        <Toolbar toolType={"piano"} toolName={"Piano"} nodeName={"piano with stuff to the right"} version={version}>
            <button className={"btn btn-warning"}>Click me</button>
        </Toolbar>
        <br/>
        <Toolbar toolType={"piano"} toolName={"Piano"} nodeName={"piano for light themed bar"} dark={false} version={version}/>
        <br/>
        <Toolbar toolType={"piano"} toolName={"Piano"} nodeName={"piano with logo"} toolImage={version === 2 ? '/piano-favicon.png' : '/piano_logo.png'} version={version}/>
        <br/>
        <Toolbar toolType={"piano"} nodeName={"piano with logo and no text"} toolImage={version === 2 ? '/piano-favicon.png' : '/piano_logo.png'} version={version}/>
        <br/>
        <Toolbar toolType={"piano"} toolName={"Piano"} nodeName={"piano with onChangeName callback"} onChangeName={(name) => console.log(`name: ${name}`)} version={version}/>
        <br/>
        <Toolbar toolType={"piano"} toolName={"Piano"} nodeName={"piano with unSavedChanges but no onSave callback"} unSavedChanges={true} version={version}/>
        <br/>
        <Toolbar toolType={"piano"} toolName={"Piano"} nodeName={"piano with unSavedChanges and onSave callback"} unSavedChanges={true} onSave={async () => { await sleep(2000); return false }} version={version}/>
        <br/>
        <Toolbar toolType={"piano"} toolName={"Piano"} nodeName={"sticky top"} sticky version={version}/>
        <hr/>

        <Toolbar toolType={"banjo"} version={version}/>
        <br/>
        <Toolbar toolType={"concerto"} classNames={{button: "btn-warning px-3"}} version={version}>
            <button className={"btn btn-warning"}>Click me</button>
        </Toolbar>
        <br/>
        <Toolbar toolType={"kimono"} version={version}/>
        <br/>
        <Toolbar toolType={"python"} version={version}/>
        <br/>
        <Toolbar toolType={"r"} version={version}/>
        <br/>
        <Toolbar toolType={"solo"} version={version}/>
        <br/>
        <Toolbar toolType={"soprano"} version={version}/>
        <br/>
        <Toolbar toolType={"sumo"} version={version}/>
        <br/>
        <Toolbar toolType={"tango"} version={version}/>
        <br/>
        <Toolbar toolType={"tempo"} version={version}/>

        <hr/>

        <Toolbar toolType={"velcro-source-db"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-source-fhir"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-source-csv"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-source-fhir-json"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-de-identify"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-staging"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-transform"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-qc"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-store"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-sql"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-sql"} nodeName={"with custom default name"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-concatenate"} version={version}/>
        <br/>
        <Toolbar toolType={"velcro-demographics"} version={version}/>

    </div>
}

export default App
