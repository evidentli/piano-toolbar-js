import React from 'react'
import PropTypes from 'prop-types'
// FONT-AWESOME     https://github.com/FortAwesome/react-fontawesome#build-a-library-to-reference-icons-throughout-your-app-more-conveniently
import { library } from '@fortawesome/fontawesome-svg-core'
import { faList, faMapSigns, faPencilAlt, faUser, faUsers, faDatabase, faFire, faEraser, faCog, faCode, faTimes, faQuestion, faSpinner, faTable, faFileImport } from '@fortawesome/free-solid-svg-icons'
import { faClone, faFileAlt, faSquare, faSave, faClock } from '@fortawesome/free-regular-svg-icons'
import { faPython, faRProject } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { TOOL_PROPS } from './config'
import { Text } from './components/Text'
import { NodeName } from './components/NodeName'
import { Icon } from './components/Icon'
import { CloseDialog } from './components/CloseDialog';
import { ToolTip } from './components/ToolTip';
import './styles.module.css'

library.add(
    faPencilAlt,
    faUser, faUsers, faMapSigns, faList,
    faSquare, faClone, faFileAlt,
    faRProject, faPython,
    faDatabase, faFire, faEraser, faCog, faSave, faCode,
    faTimes, faQuestion, faSpinner, faClock, faTable, faFileImport
);

// TODO fix react/react-dom dependency problem that prevents the use of hooks
// ... multiple version of the packages are being installed and are conflicting
// ... with the main app versions

const Button = ({path, target, className="", children, tooltip, onClick, ...props}) => {
    const btn = <a href={path} target={target} className={`btn ${className}`} onClick={onClick} role="button" {...props}>{children}</a>;
    if (tooltip)
        return <ToolTip id={'piano-toolbar-js-btn'} description={tooltip} placement={"bottom"}>{btn}</ToolTip>;
    return btn;
};

const closeNode = () => {
    if (window.location !== window.parent.location) {
        // if windows parent is not itself (i.e. it's running as an iframe in cello),
        // then send a 'close' message to the parent
        const message = { close: true }
        window.parent.postMessage(message, '*')
    }
}

const deepCopy = (obj) => {
    return obj ? JSON.parse(JSON.stringify(obj)) : obj;
};

export class Toolbar extends React.Component {

    constructor(props) {
        super(props);

        this.setStateAsync = this.setStateAsync.bind(this);
        this.handleClickClose = this.handleClickClose.bind(this);
        this.handleSaveClose = this.handleSaveClose.bind(this);
        this.handleDiscardClose = this.handleDiscardClose.bind(this);
        this.handleCancelClose = this.handleCancelClose.bind(this);

        this.state = {
            showCloseDialog: false,
            isSaving: false,
        }
    }

    setStateAsync(state) {
        return new Promise(resolve => {
            this.setState(deepCopy(state), resolve);
        });
    }

    handleClickClose() {
        const {unSavedChanges} = this.props;
        if (!unSavedChanges)
            return closeNode();
        this.setState({showCloseDialog: true});
    }

    async handleSaveClose() {
        await this.setStateAsync({isSaving: true});
        const {onSave} = this.props;

        let saved = false;
        if (onSave && typeof onSave === 'function') {
            saved = await onSave().catch(() => false);
        }
        await this.setStateAsync({isSaving: false});
        if (saved || saved === undefined) {
            closeNode();
            return;
        }
        this.setState({showCloseDialog: false});
    }

    handleDiscardClose() {
        closeNode();
    }

    handleCancelClose() {
        this.setState({showCloseDialog: false});
    }

    render() {
        const { showCloseDialog, isSaving } = this.state;
        const { toolType, toolName, toolImage, nodeName, unSavedChanges, dark=true, onChangeName, onSave, children, sticky=false, version=1 } = this.props;
        let classNames = this.props.classNames;
        if (!classNames)
            classNames = version === 2 ? {button: "btn-primary"} : {button: "btn-light"};
        let canEditName = true;
        let title = toolName;
        let name = nodeName;
        let icon, documentationButton;
        if (TOOL_PROPS.hasOwnProperty(toolType)) {
            title = TOOL_PROPS[toolType].title;
            name = nodeName || TOOL_PROPS[toolType].name;
            icon = TOOL_PROPS[toolType].icon;
            canEditName = TOOL_PROPS[toolType].edit === undefined ? true : !!TOOL_PROPS[toolType].edit;
            if (TOOL_PROPS[toolType].documentationLink) {
                documentationButton = <Button id={'piano-toolbar-help-btn'} target={"_blank"} path={TOOL_PROPS[toolType].documentationLink} className={`${classNames.button || ''} ml-2`} tooltip={"Help"}>
                    <FontAwesomeIcon className={'fa-fw'} icon={"question"}/>
                </Button>;
            }
        }

        const brandBorderRightWidth = version === 2 ? '2' : '1';
        const brandBorderRightColour = version === 2 ? 'var(--primary)' : `rgba(${dark ? '255, 255, 255' : '0, 0, 0'}, 0.7)`;
        const styles = {
            nav: {
                height: '60px',
                borderBottom: '3px solid var(--primary)',
                zIndex: sticky ? 1030 : 'initial',
            },
            brand: {
                paddingRight: "2rem",
                borderRight: canEditName ? `${brandBorderRightWidth}px solid ${brandBorderRightColour}` : '',
            },
            title: {
                minWidth: "80px",
                marginLeft: toolImage ? "1rem" : ""
            }
        };
        const handleSave = !!onSave ? this.handleSaveClose : undefined;

        const navClassName = version === 2 ?
            'navbar-light text-primary bg-white' :
            `navbar-${dark ? 'dark' : 'light'} bg-primary`;

        return <React.Fragment>
            <nav className={`navbar ${navClassName} tool-bar align-items-center ${sticky ? 'sticky-top' : ''}`} style={styles.nav}>
                {(toolImage || title) && <div className={`navbar-brand mb-0 h1 d-flex align-items-center`} style={styles.brand}>
                    <Icon toolName={title} toolImage={toolImage} toolIcon={icon} className={version === 2 ? 'text-primary' : ''}/>
                    {title && <Text style={styles.title} className={version === 2 ? 'text-primary' : ''}>{title}</Text>}
                </div>}
                <div className={'mr-auto'}>
                    {canEditName && <NodeName defaultName={name} dark={dark} onChangeName={onChangeName} version={version}/>}
                </div>
                {children}
                {documentationButton}
                <Button id={'piano-toolbar-close-btn'} path={"#close"} className={`${classNames.button || ''} ml-2`} onClick={this.handleClickClose} tooltip={"Close"}>
                    <FontAwesomeIcon className={'fa-fw'} icon={"times"}/>
                </Button>
            </nav>
            <CloseDialog show={showCloseDialog} unSavedChanges={unSavedChanges} isSaving={isSaving}
                         onSave={handleSave}
                         onClose={this.handleDiscardClose}
                         onCancel={this.handleCancelClose}/>
        </React.Fragment>
    }
}

Toolbar.propTypes = {
    toolType: PropTypes.string.isRequired,      // may be used for auto creating links to documentation etc.
    toolName: PropTypes.string,
    toolImage: PropTypes.string,
    nodeName: PropTypes.string,
    dark: PropTypes.bool,
    onChangeName: PropTypes.func,
    classNames: PropTypes.object,
    unSavedChanges: PropTypes.bool,
    onSave: PropTypes.func,
    sticky: PropTypes.bool,
    version: PropTypes.oneOf([1, 2]),
}
