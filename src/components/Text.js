import React from 'react'

import {FONT_FAMILY} from '../config';

export const Text = ({ className, style = {}, onClick, children }) => (
    <span className={className} style={{ ...style, fontFamily: FONT_FAMILY }} onClick={onClick}>
        {children}
    </span>
)
