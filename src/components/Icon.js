import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const Icon = ({ toolName, toolImage, toolIcon, className="" }) => {
    const style = { marginRight: toolName ? '1rem' : '' }
    if (toolImage) {
        return <img src={toolImage} height="30" alt="" style={style}/>
    } else if (toolIcon) {
        return <FontAwesomeIcon icon={toolIcon} className={`fa-fw ${className}`} style={style}/>
    }
    return null
}
