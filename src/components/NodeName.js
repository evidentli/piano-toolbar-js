import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Text } from './Text';
import { FONT_FAMILY } from '../config'

export class NodeName extends React.Component {

    constructor(props) {
        super(props)
        this.handleMessageEvent = this.handleMessageEvent.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.handleKeyPress = this.handleKeyPress.bind(this)
        this.handleChange = this.handleChange.bind(this)

        this.state = {
            defaultName: props['defaultName'] || '',
            name: props['defaultName'],
            isEditing: false,
            isHovering: false
        }
    }

    componentDidMount() {
        window.addEventListener('message', this.handleMessageEvent)
    }

    componentWillUnmount() {
        window.removeEventListener('message', this.handleMessageEvent)
    }

    handleMessageEvent(event) {
        if (event && event.data && event.data.hasOwnProperty('name') && event.data.name && Object.keys(event.data).length === 1) {
            const name = event.data.name;
            this.setState({ name: name });
            const {onChangeName} = this.props;
            if (onChangeName) {
                onChangeName(name);
            }
        }
    }

    handleClose() {
        let name = `${this.state.name || ''}`;
        if (!name || !name.trim().length) {
            name = this.state.defaultName;
        }
        if (window.location !== window.parent.location) {
            // if windows parent is not itself (i.e. it's running as an iframe in cello),
            // then send the name to parent
            const message = { name: name }
            window.parent.postMessage(message, '*')
        }
        this.setState({ isEditing: false, name });
        const {onChangeName} = this.props;
        if (onChangeName) {
            onChangeName(name);
        }
    }

    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.handleClose()
        }
    }

    handleChange(e) {
        this.setState({ name: e.target.value });
    }

    render() {
        const { isEditing, isHovering, name } = this.state
        const { dark, version } = this.props

        const styles = {
            container: {
                cursor: isHovering ? 'pointer' : '',
                border: isEditing ? '' : isHovering ? `1px solid rgba(${dark ? '255, 255, 255' : '0, 0, 0'}, 0.7)` : '',
            },
            input: {
                fontFamily: FONT_FAMILY,
            },
            name: {
                opacity: 0.85, color: dark ? '#fff' : '#000',
            },
            icon: {
                marginLeft: '5px',
            }
        }
        const borderVariant = version === 2 ? 'white' : 'primary';
        const borderVariantHover = version === 2 ? 'primary' : 'white';

        return <div className={` rounded border border-${isEditing ? '' : isHovering ? borderVariantHover : `${borderVariant}`}`}
                    style={styles.container}
                    onClick={() => this.setState({ isEditing: true })}
                    onMouseEnter={() => this.setState({ isHovering: true })}
                    onMouseLeave={() => this.setState({ isHovering: false })}>
            {isEditing ?
                <input className="form-control" value={name} onKeyPress={this.handleKeyPress}
                       onChange={this.handleChange}
                       style={styles.input}
                       onBlur={this.handleClose} autoFocus={true}/> :
                <Text className={`navbar-text mx-3 ${version === 2 ? 'text-primary' : ''}`}
                      style={styles.name}>
                    {name} <FontAwesomeIcon style={styles.icon} size={'sm'} icon="pencil-alt"/>
                </Text>
            }
        </div>
    }

}
