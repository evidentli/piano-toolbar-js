import React from "react";
import {Tooltip, OverlayTrigger} from "react-bootstrap";

export const ToolTip = ({id, placement="top", description, children, rootClose=true, trigger=['hover']}) => {
    if (!description)
        return children;

    const overlay = <Tooltip id={id}>
        {description}
    </Tooltip>;

    return (
        <OverlayTrigger placement={placement} overlay={overlay} rootClose={rootClose} trigger={trigger}>
                {children}
        </OverlayTrigger>
    );
};
