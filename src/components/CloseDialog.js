import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import {Modal} from './Modal';

const Button = ({className, onClick, children, ...props}) => {
    return <button className={`btn ${className || ''}`} onClick={onClick} {...props}>{children}</button>
}

export class CloseDialog extends React.Component {

    render() {
        const {show, isSaving, onClose, onCancel, onSave} = this.props;
        const disabled = isSaving;
        return <Modal id={'piano-toolbar-close-modal'} show={show}
                      onClose={onCancel}
                      body={"You have unsaved changes! How do you want to proceed?"}
                      footer={
                          <React.Fragment>
                              {!!onSave && <Button id={'piano-toolbar-close-modal-save-btn'} onClick={onSave} className="btn-primary" disabled={disabled}>
                                  {isSaving ? <span>Saving <FontAwesomeIcon icon={'spinner'} spin/></span> : 'Save & Close'}
                              </Button>}
                              <Button id={'piano-toolbar-close-modal-discard-btn'} onClick={onClose} className={"btn-danger"} disabled={disabled}>
                                  Discard & Close
                              </Button>
                              <Button id={'piano-toolbar-close-modal-cancel-btn'} onClick={onCancel} className={"btn-secondary"} disabled={disabled}>
                                  Cancel
                              </Button>
                          </React.Fragment>
                      }/>
    }
}
