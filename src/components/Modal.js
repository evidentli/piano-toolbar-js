import React from 'react';
import PropTypes from 'prop-types'
import {Modal as _Modal} from 'react-bootstrap';

export const Modal = ({id, show, header, body, footer, onClose}) => (
    <_Modal id={id} show={show} onHide={onClose}>
        {!!header && <_Modal.Header>{header}</_Modal.Header>}
        <_Modal.Body>{body}</_Modal.Body>
        {!!footer && <_Modal.Footer>{footer}</_Modal.Footer>}
    </_Modal>
);

Modal.propTypes = {
    show: PropTypes.bool,
    header: PropTypes.node,
    body: PropTypes.node.isRequired,
    footer: PropTypes.node,
    onClose: PropTypes.func,
}
