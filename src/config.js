export const TOOL_PROPS = {
    'solo': {
        title: 'solo',
        name: 'solo',
        icon: { prefix: 'fas', iconName: 'user' },
        documentationLink: '/docs/analytic_tools/solo.html'
    },
    'concerto': {
        title: 'concerto',
        name: 'concerto',
        icon: { prefix: 'fas', iconName: 'users' },
        documentationLink: '/docs/analytic_tools/concerto.html'
    },
    'tango': {
        title: 'tango',
        name: 'tango',
        icon: { prefix: 'fas', iconName: 'map-signs' },
        documentationLink: '/docs/analytic_tools/tango.html'
    },
    'soprano': {
        title: 'soprano',
        name: 'soprano',
        icon: { prefix: 'fas', iconName: 'list' },
        documentationLink: '/docs/analytic_tools/soprano.html'
    },
    'sumo': {
        title: 'sumo',
        name: 'sumo',
        icon: { prefix: 'far', iconName: 'square' },
        documentationLink: '/docs/analytic_tools/sumo.html'
    },
    'kimono': {
        title: 'kimono',
        name: 'kimono',
        icon: { prefix: 'far', iconName: 'clone' },
        documentationLink: '/docs/analytic_tools/kimono.html'
    },
    'tempo': {
        title: 'tempo',
        name: 'tempo',
        icon: { prefix: 'far', iconName: 'clock' }
        // documentationLink: '/docs/analytic_tools/tempo.html' TODO
    },
    'r': {
        title: 'R',
        name: 'r',
        icon: { prefix: 'fab', iconName: 'r-project' },
        documentationLink: '/docs/analytic_tools/r.html#r-script-parameters'
    },
    'python': {
        title: 'python',
        name: 'python',
        icon: { prefix: 'fab', iconName: 'python' },
        documentationLink: '/docs/analytic_tools/python.html#python-script-parameters'
    },
    'python-velcro': {
        title: 'python',
        name: 'python',
        icon: { prefix: 'fab', iconName: 'python' },
        documentationLink: '/docs/ingress_tools/python.html',
    },
    'banjo': {
        title: 'banjo',
        name: 'banjo',
        icon: { prefix: 'far', iconName: 'file-alt' }
    },
    'velcro-source-db': {
        title: 'ODBC connector',
        name: "source",
        icon: { prefix: 'fas', iconName: 'database' },
        edit: false,
        documentationLink: '/docs/ingress_tools/db_connector.html',
    },
    'velcro-source-fhir': {
        title: 'FHIR connector',
        name: "FHIR",
        icon: { prefix: 'fas', iconName: 'fire' },
        edit: false,
        documentationLink: '/docs/ingress_tools/db_connector.html',
    },
    'velcro-source-csv': {
        title: 'CSV upload',
        name: "CSV",
        icon: { prefix: 'fas', iconName: 'file-import' },
        edit: false,
        //documentationLink: '/docs/ingress_tools/file_upload.html', // TODO
    },
    'velcro-source-fhir-json': {
        title: 'JSON upload',
        name: "FHIR JSON",
        icon: { prefix: 'fas', iconName: 'file-import' },
        edit: false,
        //documentationLink: '/docs/ingress_tools/file_upload.html', // TODO
    },
    'velcro-de-identify': {
        title: 'De-Identification',
        name: "de-identify",
        icon: { prefix: 'fas', iconName: 'eraser' },
        documentationLink: '/docs/ingress_tools/de_identify.html',
    },
    'velcro-staging': {
        title: 'Staging',
        name: "staging",
        icon: { prefix: 'far', iconName: 'clone' },
        documentationLink: '/docs/ingress_tools/staging.html',
    },
    'velcro-transform': {
        title: 'Transform',
        name: "transform",
        icon: { prefix: 'fas', iconName: 'cog' },
        documentationLink: '/docs/ingress_tools/transform.html',
    },
    'velcro-qc': {
        title: 'Quality Control',
        name: "qc",
        icon: { prefix: 'far', iconName: 'file-alt' },
        documentationLink: '/docs/ingress_tools/qc.html',
    },
    'velcro-store': {
        title: 'Store',
        name: "store",
        icon: { prefix: 'far', iconName: 'save' },
        documentationLink: '/docs/ingress_tools/store.html',
    },
    'velcro-sql': {
        title: 'SQL',
        name: "sql",
        icon: { prefix: 'fas', iconName: 'code' },
        documentationLink: '/docs/ingress_tools/sql.html',
    },
    'velcro-concatenate': {
        title: 'Concatenate',
        name: "concatenate",
        icon: { prefix: 'fas', iconName: 'code' },
        // documentationLink: '/docs/ingress_tools/concatenate.html', // TODO
    },
    'velcro-demographics': {
        title: 'Demographics',
        name: 'demographics',
        icon: { prefix: 'fas', iconName: 'user'},
        // documentationLink: '/docs/ingress_tools/demographics.html',
    },
    'velcro-regex': {
        title: 'Regex',
        name: "regex",
        icon: { prefix: 'fas', iconName: 'code' },
        // documentationLink: '/docs/ingress_tools/regex.html', // TODO
    },
    'velcro-math-operation': {
        title: 'Math Operation',
        name: "math-operation",
        icon: { prefix: 'fas', iconName: 'code' },
        // documentationLink: '/docs/ingress_tools/math_op.html', // TODO
    }
}

export const FONT_FAMILY = 'Poppins, Questrial, Nunito Sans, sans-serif';
